package main

import (
    "context"
    "flag"
    "fmt"
    "path/filepath"
    "encoding/json"
    "log"
    "github.com/Jeffail/gabs"

    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/tools/clientcmd"
    "k8s.io/client-go/util/homedir"
)

func getPodsAll(clientset *kubernetes.Clientset) {
  pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
  if err != nil {
    panic(err.Error())
  }

  for _, pod := range pods.Items {
    bPod, err := json.MarshalIndent(pod, "", "  ")
    if err != nil {
    	log.Fatal(err)
    }
    parsedPod, err := gabs.ParseJSON(bPod)
    value, _ := parsedPod.Path("metadata.name").Data().(string)
    fmt.Println(value)
  }

}

func main() {
  // the whole login was taken from https://github.com/kubernetes/client-go/blob/master/examples/create-update-delete-deployment/main.go
  var kubeconfig *string
  if home := homedir.HomeDir(); home != "" {
    kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
  } else {
    kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
  }
  flag.Parse()
  
  config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
  if err != nil {
    panic(err)
  }
  clientset, err := kubernetes.NewForConfig(config)
  if err != nil {
    panic(err)
  }
  getPodsAll(clientset)
}
